<?php

define("SITE_NAME", "Baska");
define("SITE_URL", "http://baska.lv/");
define("EMAIL", "info@baska.lv");
define("PHONE", "+371 29764763");
define("ADDRESS", "Maskavas street 497, Rumbula, Stopiņu novads, LV-2121 
Openning hours 9:00-17:00");

define("SOCIALS", [
    'facebook' => 'http://facebook.com',
    'twitter' => 'http://twitter.com',
    'behance' => 'http://behance.com',
    'instagram' => 'http://instagram.com',
    'deviantart' => 'http://deviantart.com',
]);