<section class="l-section  height_large color_primary with_img parallax_fixed" id="portfolio">

    <div class="l-section-img"
         style="background-image: url(./img/colorful-266993_1920.jpg); background-position: 50% 50%;"
         data-img-width="1920" data-img-height="1276"></div>
    <div class="l-section-overlay" style="background:rgba(0,0,0,0.4)"></div>
    <div class="l-section-h i-cf">
        <div class="g-cols via_grid cols_1 laptops-cols_inherit tablets-cols_inherit mobiles-cols_1 valign_top type_default">
            <div class=" vc_column_container">
                <div class="vc_column-inner">
                    <div class="wpb_text_column">
                        <div class="wpb_wrapper"><h2 style="text-align: center;">Latest Projects</h2>
                        </div>
                    </div>
                    <div class="w-separator size_small"></div>
                    <div class="w-grid type_masonry layout_22849 cols_3 with_isotope ratio_4x3" id="us_grid_1"
                         data-filterable="true">
                        <style>
                            #us_grid_1 .w-grid-item {
                                padding: 2px
                            }

                            #us_grid_1 .w-grid-list {
                                margin: -2px
                            }

                            .w-grid + #us_grid_1 .w-grid-list, .w-grid-none + #us_grid_1 .w-grid-list {
                                margin-top: 2px
                            }

                            @media (max-width: 899px) {
                                .l-section-overlay{
                                    max-width: 100%;
                                }
                                .type_masonry{
                                    overflow: hidden;
                                }
                                #us_grid_1 .w-grid-item {
                                    width: 50%
                                }

                                #us_grid_1 .w-grid-item.size_2x1, #us_grid_1 .w-grid-item.size_2x2 {
                                    width: 100%
                                }
                            }

                            @media (max-width: 599px) {
                                #us_grid_1 .w-grid-list {
                                    margin: 0
                                }

                                #us_grid_1 .w-grid-item {
                                    width: 100%;
                                    padding: 0;
                                    margin-bottom: 2px
                                }
                            }

                            .layout_22849 .w-grid-item-h:before {
                                padding-bottom: 75%
                            }

                            @media (min-width: 600px) {
                                .layout_22849 .w-grid-item.size_1x2 .w-grid-item-h:before {
                                    padding-bottom: calc(150% + 2px + 2px)
                                }

                                .layout_22849 .w-grid-item.size_2x1 .w-grid-item-h:before {
                                    padding-bottom: calc(37.5% - 2px * 0.75)
                                }

                                .layout_22849 .w-grid-item.size_2x2 .w-grid-item-h:before {
                                    padding-bottom: calc(75% - 2px * -0.5)
                                }
                            }

                            .layout_22849 .w-grid-item-h {
                            }

                            .layout_22849 .usg_post_custom_field_1 {
                                transition-duration: 0.4s;
                                transform: scale(0) translate(0%, 0%);
                                opacity: 0
                            }

                            .layout_22849 .w-grid-item-h:hover .usg_post_custom_field_1 {
                                transform: scale(1.5) translate(0%, 0%);
                                opacity: 1
                            }

                            .layout_22849 .usg_vwrapper_1 {
                                transition-duration: 0.4s;
                                transform: scale(0) translate(0%, 0%);
                                opacity: 0
                            }

                            .layout_22849 .w-grid-item-h:hover .usg_vwrapper_1 {
                                transform: scale(1) translate(0%, 0%);
                                opacity: 1
                            }

                            .layout_22849 .usg_post_image_1 {
                                position: absolute !important;
                                top: 0 !important;
                                right: 0 !important;
                                bottom: 0 !important;
                                left: 0 !important
                            }

                            .layout_22849 .usg_post_custom_field_1 {
                                position: absolute !important;
                                top: 0 !important;
                                right: 0 !important;
                                bottom: 0 !important;
                                left: 0 !important;
                                font-family: var(--font-body) !important;
                                border-radius: 50% !important;
                                padding: 2rem !important;
                                background: inherit !important
                            }

                            .layout_22849 .usg_vwrapper_1 {
                                position: absolute !important;
                                top: 0 !important;
                                right: 0 !important;
                                bottom: 0 !important;
                                left: 0 !important;
                                padding: 2rem !important
                            }

                            .layout_22849 .usg_post_title_1 {
                                font-family: var(--font-h1) !important;
                                font-size: 1.2rem !important;
                                color: inherit !important
                            }</style>
                        <div class="w-grid-list" style="position: relative; height: 603.968px;">
                            <article
                                class="w-grid-item size_1x1 post-331 us_portfolio type-us_portfolio status-publish has-post-thumbnail hentry us_portfolio_category-photography"
                                data-id="331" style="position: absolute; left: 0px; top: 0px;">
                                <div class="w-grid-item-h" style="background:#7e57c2;color:#ffffff;">
                                    <a class="w-grid-item-anchor"
                                       href="/portfolio/project-slider/"
                                       rel="bookmark" aria-label="Single Project – Slider"><span
                                            class="ripple-container"></span></a>
                                    <div class="w-post-elm post_image usg_post_image_1 stretched"><img
                                            width="1024" height="683" src="./picjumbo.com_HNCK0082-1024x683.jpg"
                                            class="attachment-large size-large wp-post-image" alt=""
                                            loading="lazy"
                                            srcset="https://zephyr.us-themes.com/wp-content/uploads/picjumbo.com_HNCK0082-1024x683.jpg 1024w, https://zephyr.us-themes.com/wp-content/uploads/picjumbo.com_HNCK0082-600x400.jpg 600w, https://zephyr.us-themes.com/wp-content/uploads/picjumbo.com_HNCK0082.jpg 1920w"
                                            sizes="(max-width: 1024px) 100vw, 1024px"></div>
                                    <div class="w-post-elm post_custom_field usg_post_custom_field_1 type_text has_border_radius">
                                        <span class="w-post-elm-value"></span></div>
                                    <div class="w-vwrapper usg_vwrapper_1 align_center valign_middle"><h2
                                            class="w-post-elm post_title usg_post_title_1 entry-title color_link_inherit">
                                            Single Project – Slider</h2></div>
                                </div>
                            </article>
                            <article
                                class="w-grid-item size_1x1 post-332 us_portfolio type-us_portfolio status-publish has-post-thumbnail hentry us_portfolio_category-illustration us_portfolio_category-photography"
                                data-id="332" style="position: absolute; left: 401.328px; top: 0px;">
                                <div class="w-grid-item-h" style="background:#ffd54f;color:#2b2420;">
                                    <a class="w-grid-item-anchor"
                                       href="/portfolio/creative-project-slider/"
                                       rel="bookmark" aria-label="Creative Project – Slider"><span
                                            class="ripple-container"></span></a>
                                    <div class="w-post-elm post_image usg_post_image_1 stretched"><img
                                            width="1024" height="683"
                                            src="./shutterstock_112330751-1024x683.jpg"
                                            class="attachment-large size-large wp-post-image" alt=""
                                            loading="lazy"
                                            srcset="https://zephyr.us-themes.com/wp-content/uploads/shutterstock_112330751-1024x683.jpg 1024w, https://zephyr.us-themes.com/wp-content/uploads/shutterstock_112330751-600x400.jpg 600w, https://zephyr.us-themes.com/wp-content/uploads/shutterstock_112330751.jpg 1920w"
                                            sizes="(max-width: 1024px) 100vw, 1024px"></div>
                                    <div class="w-post-elm post_custom_field usg_post_custom_field_1 type_text has_border_radius">
                                        <span class="w-post-elm-value"></span></div>
                                    <div class="w-vwrapper usg_vwrapper_1 align_center valign_middle"><h2
                                            class="w-post-elm post_title usg_post_title_1 entry-title color_link_inherit">
                                            Creative Project – Slider</h2></div>
                                </div>
                            </article>
                            <article
                                class="w-grid-item post-337 us_portfolio type-us_portfolio status-publish has-post-thumbnail hentry us_portfolio_category-illustration us_portfolio_category-web-design-2"
                                data-id="337" style="position: absolute; left: 802.656px; top: 0px;">
                                <div class="w-grid-item-h" style="background:#fff4ea;color:#38291d;">
                                    <a class="w-grid-item-anchor"
                                       href="/portfolio/project-image/"
                                       rel="bookmark" aria-label="Single Project – Image"><span
                                            class="ripple-container"></span></a>
                                    <div class="w-post-elm post_image usg_post_image_1 stretched"><img
                                            width="1024" height="683" src="./picjumbo.com_IMG_6850-1024x683.jpg"
                                            class="attachment-large size-large wp-post-image" alt=""
                                            loading="lazy"
                                            srcset="https://zephyr.us-themes.com/wp-content/uploads/picjumbo.com_IMG_6850-1024x683.jpg 1024w, https://zephyr.us-themes.com/wp-content/uploads/picjumbo.com_IMG_6850-600x400.jpg 600w, https://zephyr.us-themes.com/wp-content/uploads/picjumbo.com_IMG_6850.jpg 1920w"
                                            sizes="(max-width: 1024px) 100vw, 1024px"></div>
                                    <div class="w-post-elm post_custom_field usg_post_custom_field_1 type_text has_border_radius">
                                        <span class="w-post-elm-value"></span></div>
                                    <div class="w-vwrapper usg_vwrapper_1 align_center valign_middle"><h2
                                            class="w-post-elm post_title usg_post_title_1 entry-title color_link_inherit">
                                            Single Project – Image</h2></div>
                                </div>
                            </article>
                            <article
                                class="w-grid-item size_1x1 post-340 us_portfolio type-us_portfolio status-publish has-post-thumbnail hentry us_portfolio_category-photography"
                                data-id="340" style="position: absolute; left: 0px; top: 301.984px;">
                                <div class="w-grid-item-h" style="background:#ff5d44;color:#ffffff;">
                                    <a class="w-grid-item-anchor"
                                       href="/portfolio/creative-project-image/"
                                       rel="bookmark" aria-label="Creative Project – Image"><span
                                            class="ripple-container"></span></a>
                                    <div class="w-post-elm post_image usg_post_image_1 stretched"><img
                                            width="1024" height="768" src="./port-6-1024x768.jpg"
                                            class="attachment-large size-large wp-post-image" alt=""
                                            loading="lazy"
                                            srcset="https://zephyr.us-themes.com/wp-content/uploads/2014/09/port-6-1024x768.jpg 1024w, https://zephyr.us-themes.com/wp-content/uploads/2014/09/port-6-600x450.jpg 600w, https://zephyr.us-themes.com/wp-content/uploads/2014/09/port-6.jpg 1600w"
                                            sizes="(max-width: 1024px) 100vw, 1024px"></div>
                                    <div class="w-post-elm post_custom_field usg_post_custom_field_1 type_text has_border_radius">
                                        <span class="w-post-elm-value"></span></div>
                                    <div class="w-vwrapper usg_vwrapper_1 align_center valign_middle"><h2
                                            class="w-post-elm post_title usg_post_title_1 entry-title color_link_inherit">
                                            Creative Project – Image</h2></div>
                                </div>
                            </article>
                            <article
                                class="w-grid-item size_1x1 post-341 us_portfolio type-us_portfolio status-publish has-post-thumbnail hentry us_portfolio_category-video-2"
                                data-id="341" style="position: absolute; left: 401.328px; top: 301.984px;">
                                <div class="w-grid-item-h" style="background:#7cb342;color:#ffffff;">
                                    <a class="w-grid-item-anchor"
                                       href="/portfolio/project-video/"
                                       rel="bookmark" aria-label="Single Project – Video"><span
                                            class="ripple-container"></span></a>
                                    <div class="w-post-elm post_image usg_post_image_1 stretched"><img
                                            width="1024" height="683" src="./picjumbo.com_IMG_9083-1024x683.jpg"
                                            class="attachment-large size-large wp-post-image" alt=""
                                            loading="lazy"
                                            srcset="https://zephyr.us-themes.com/wp-content/uploads/picjumbo.com_IMG_9083-1024x683.jpg 1024w, https://zephyr.us-themes.com/wp-content/uploads/picjumbo.com_IMG_9083-600x400.jpg 600w, https://zephyr.us-themes.com/wp-content/uploads/picjumbo.com_IMG_9083.jpg 1920w"
                                            sizes="(max-width: 1024px) 100vw, 1024px"></div>
                                    <div class="w-post-elm post_custom_field usg_post_custom_field_1 type_text has_border_radius">
                                        <span class="w-post-elm-value"></span></div>
                                    <div class="w-vwrapper usg_vwrapper_1 align_center valign_middle"><h2
                                            class="w-post-elm post_title usg_post_title_1 entry-title color_link_inherit">
                                            Single Project – Video</h2></div>
                                </div>
                            </article>
                            <article
                                class="w-grid-item size_1x1 post-338 us_portfolio type-us_portfolio status-publish has-post-thumbnail hentry us_portfolio_category-photography us_portfolio_category-video-2"
                                data-id="338" style="position: absolute; left: 802.656px; top: 301.984px;">
                                <div class="w-grid-item-h" style="background:#41baae;color:#ffffff;">
                                    <a class="w-grid-item-anchor"
                                       href="/portfolio/creative-project-video/"
                                       rel="bookmark" aria-label="Creative Project – Video"><span
                                            class="ripple-container"></span></a>
                                    <div class="w-post-elm post_image usg_post_image_1 stretched"><img
                                            width="1024" height="683" src="./picjumbo.com_HNCK2634-1024x683.jpg"
                                            class="attachment-large size-large wp-post-image" alt=""
                                            loading="lazy"
                                            srcset="https://zephyr.us-themes.com/wp-content/uploads/picjumbo.com_HNCK2634-1024x683.jpg 1024w, https://zephyr.us-themes.com/wp-content/uploads/picjumbo.com_HNCK2634-600x400.jpg 600w, https://zephyr.us-themes.com/wp-content/uploads/picjumbo.com_HNCK2634.jpg 1920w"
                                            sizes="(max-width: 1024px) 100vw, 1024px"></div>
                                    <div class="w-post-elm post_custom_field usg_post_custom_field_1 type_text has_border_radius">
                                        <span class="w-post-elm-value"></span></div>
                                    <div class="w-vwrapper usg_vwrapper_1 align_center valign_middle"><h2
                                            class="w-post-elm post_title usg_post_title_1 entry-title color_link_inherit">
                                            Creative Project – Video</h2></div>
                                </div>
                            </article>
                        </div>
                        <div class="w-grid-preloader">
                            <div class="g-preloader type_1">
                                <div></div>
                            </div>
                        </div>
                    </div>
                    <div class="w-separator size_medium"></div>
                    <div class="w-btn-wrapper align_center"><a class="w-btn us-btn-style_6 icon_atleft"
                                                               href="/portfolio/"><i
                                class="material-icons">arrow_forward</i><span class="w-btn-label">View Full Portfolio</span><span
                                class="ripple-container"></span></a></div>
                </div>
            </div>
        </div>
    </div>
</section>