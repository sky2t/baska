<section class="l-section height_large color_alternate" id="responsive">

    <div class="l-section-h i-cf">
        <div class="g-cols via_grid cols_1 laptops-cols_inherit tablets-cols_inherit mobiles-cols_1 valign_top type_default">
            <div class=" vc_column_container us_animate_afb start">
                <div class="vc_column-inner">
                    <div class="wpb_text_column">
                        <div class="wpb_wrapper"><h2 style="text-align: center;">Details Done Right</h2>
                            <p style="text-align: center;">Limitless options beautifully packed in a brand-new
                                premium WordPress theme.</p>
                        </div>
                    </div>
                    <div class="w-separator size_large"></div>
                    <div class="g-cols  via_grid cols_2 laptops-cols_inherit tablets-cols_inherit mobiles-cols_1 valign_top type_default">
                        <div class=" vc_column_container">
                            <div class="vc_column-inner">
                                <div class="w-image align_center us_animate_afl start">
                                    <div class="w-image-h"><img src="./ipad-iphone.png"
                                                                class="attachment-large size-large" alt=""
                                                                loading="lazy"
                                                                srcset="https://zephyr.us-themes.com/wp-content/uploads/ipad-iphone.png 760w, https://zephyr.us-themes.com/wp-content/uploads/ipad-iphone-600x392.png 600w"
                                                                sizes="(max-width: 760px) 100vw, 760px"></div>
                                </div>
                            </div>
                        </div>
                        <div class=" vc_column_container us_animate_afr start">
                            <div class="vc_column-inner">
                                <div class="w-iconbox iconpos_left style_default color_contrast align_left">
                                    <div class="w-iconbox-icon" style="font-size:2rem;"><i
                                            class="material-icons">photo_camera</i></div>
                                    <div class="w-iconbox-meta"><h4 class="w-iconbox-title"
                                                                    style="font-size:20px;">Retina Ready &amp;
                                            Fully Responsive</h4>
                                        <div class="w-iconbox-text"><p>Lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="w-separator size_medium"></div>
                                <div class="w-iconbox iconpos_left style_default color_contrast align_left">
                                    <div class="w-iconbox-icon" style="font-size:2rem;"><i
                                            class="material-icons">image</i></div>
                                    <div class="w-iconbox-meta"><h4 class="w-iconbox-title"
                                                                    style="font-size:20px;">Four Awesome Header
                                            Layouts</h4>
                                        <div class="w-iconbox-text"><p>Consectetur adipiscing elit, lorem ipsum
                                                dolor sit amet.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="w-separator size_medium"></div>
                                <div class="w-iconbox iconpos_left style_default color_contrast align_left">
                                    <div class="w-iconbox-icon" style="font-size:2rem;"><i
                                            class="material-icons">forum</i></div>
                                    <div class="w-iconbox-meta"><h4 class="w-iconbox-title"
                                                                    style="font-size:20px;">Quick-to-Install and
                                            Easy-to-Use</h4>
                                        <div class="w-iconbox-text"><p>Dolor lorem ipsum sit amet, adipiscing
                                                elit consectetur.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
