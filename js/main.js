(function($) {
    $(function() {
        
        function stickyHeader(){
            var header = $('#page-header');
            if($(window).scrollTop() > 50){
                header.addClass('sticky');
            }else{
                header.removeClass('sticky');
            }
        }
        stickyHeader();

        $(document).scroll(function (){
            stickyHeader();

            if($(window).scrollTop() > 800){
                $(".w-toplink").addClass('active');
            }else{
                $(".w-toplink").removeClass('active');
            }
        });

        $('.open_on_click').click(function (e){
            $(e.currentTarget).toggleClass('opened');
        });

        $(".w-toplink").click(function (e){
            e.preventDefault();

            $('html, body').animate({
                scrollTop: 0
            }, 1000);
        });

        function detectMobile(){
            if($(window).width() < 900){
                $('.w-nav').removeClass('type_desctop').addClass('type_mobile');
            }else{
                $('.w-nav').removeClass('type_mobile').addClass('type_desctop');
            }
        }

        detectMobile();
        
        $(window).resize(function(){
            detectMobile();
            $('.w-nav-list').css('display', 'none');
        });


        //.w-nav.type_mobile
        
        $('.w-nav-icon').click(function(){
            $('.w-nav-list').css({
                height: 'auto',
                paddingTop: '0px',
                paddingBottom: '0px',
                display: 'block',
                opacity: 1,
            });
        });
    });
})(jQuery);


