<section class="l-section align_center height_huge color_primary with_img">

    <div class="l-section-img"
         style="background-image: url(./img/shutterstock_156031910.jpg);"
         data-img-width="1920" data-img-height="1280"></div>
    <div class="l-section-overlay" style="background:rgba(0,0,0,0.4)"></div>
    <div class="l-section-h i-cf">
        <div class="g-cols via_grid cols_1 laptops-cols_inherit tablets-cols_inherit mobiles-cols_1 valign_top type_default">
            <div class=" vc_column_container">
                <div class="vc_column-inner"><h1 class="w-text align_center"><span
                            class="w-text-h"><span class="w-text-value">Quality is the Best<br>
Business Plan</span></span></h1>
                    <div class="w-btn-wrapper align_none"><a
                            class="w-btn us-btn-style_1  active"
                            href="#start"><span
                                class="w-btn-label">View Features</span><span class="ripple-container"></span></a>
                    </div>
                    <div class="w-btn-wrapper align_none"><a class="w-btn us-btn-style_3"
                                                             href="#portfolio"><span
                                class="w-btn-label">View Portfolio</span><span class="ripple-container"></span></a>
                    </div>
                    <div class="w-separator size_medium"></div>
                </div>
            </div>
        </div>
    </div>
</section>