<section class="l-section  height_auto width_full">

    <div class="l-section-h i-cf">
        <div class="g-cols via_grid cols_2 laptops-cols_inherit tablets-cols_inherit mobiles-cols_1 valign_middle type_boxes"
             style="grid-gap:0rem;">
            <div class=" vc_column_container has_text_color shutterstock has_bg_color us_animate_afl start">
                <div class="vc_column-inner">
                    <div class="w-separator size_custom" style="height:200px"></div>
                </div>
            </div>
            <div class=" vc_column_container us_animate_afr start">
                <div class="vc_column-inner">
                    <div class="wpb_text_column">
                        <div class="wpb_wrapper"><h2 style="text-align: center;"><strong>Zephyr</strong> is a
                                <span class="highlight highlight_primary">creative studio</span> dedicated<br>
                                to crafting <span class="highlight highlight_primary">meaninful image</span></h2>
                        </div>
                    </div>
                    <div class="w-separator size_medium"></div>
                    <div class="g-cols  via_grid cols_2 laptops-cols_inherit tablets-cols_inherit mobiles-cols_1 valign_top type_default">
                        <div class=" vc_column_container">
                            <div class="vc_column-inner">
                                <div class="w-iconbox iconpos_left style_default color_contrast align_left">
                                    <div class="w-iconbox-icon" style="font-size:2rem;"><i
                                            class="material-icons">favorite_outline</i></div>
                                    <div class="w-iconbox-meta"><h4 class="w-iconbox-title"
                                                                    style="font-size:20px;">Clear
                                            Documentation</h4>
                                        <div class="w-iconbox-text"><p>I am text block. Click edit button to
                                                change this text. Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit ullamcorper.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="w-separator size_small"></div>
                                <div class="w-iconbox iconpos_left style_default color_contrast align_left">
                                    <div class="w-iconbox-icon" style="font-size:2rem;"><i
                                            class="material-icons">api</i></div>
                                    <div class="w-iconbox-meta"><h4 class="w-iconbox-title"
                                                                    style="font-size:20px;">Fully Featured &amp;
                                            Flexible</h4>
                                        <div class="w-iconbox-text"><p>I am text block. Click edit button to
                                                change this text. Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit ullamcorper.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" vc_column_container">
                            <div class="vc_column-inner">
                                <div class="w-iconbox iconpos_left style_default color_contrast align_left">
                                    <div class="w-iconbox-icon" style="font-size:2rem;"><i
                                            class="material-icons">send</i></div>
                                    <div class="w-iconbox-meta"><h4 class="w-iconbox-title"
                                                                    style="font-size:20px;">Optimised For
                                            Mobile</h4>
                                        <div class="w-iconbox-text"><p>I am text block. Click edit button to
                                                change this text. Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit ullamcorper.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="w-separator size_small"></div>
                                <div class="w-iconbox iconpos_left style_default color_contrast align_left">
                                    <div class="w-iconbox-icon" style="font-size:2rem;"><i
                                            class="material-icons">notifications_none</i></div>
                                    <div class="w-iconbox-meta"><h4 class="w-iconbox-title"
                                                                    style="font-size:20px;">User Friendly</h4>
                                        <div class="w-iconbox-text"><p>I am text block. Click edit button to
                                                change this text. Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit ullamcorper.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>