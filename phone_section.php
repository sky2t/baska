<section class="l-section  height_large" id="simple">

    <div class="l-section-h i-cf">
        <div class="g-cols via_grid cols_2 laptops-cols_inherit tablets-cols_inherit mobiles-cols_1 valign_middle type_default">
            <div class=" vc_column_container us_animate_afl start">
                <div class="vc_column-inner">
                    <div class="wpb_text_column">
                        <div class="wpb_wrapper"><h2>Wonderful digital things require<br>
                                a good mix of combined skills</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in purus.
                                Praesent viverra, est vitae efficitur auctor, nibh orci varius ligula, gravida
                                molestie enim justo ut diam. Praesent eget congue lacus, ut viverra neque.</p>
                        </div>
                    </div>
                    <div class="w-separator size_small"></div>
                    <div class="w-btn-wrapper align_none"><a class="w-btn us-btn-style_1"
                                                             href="#portfolio"><span
                                class="w-btn-label">See Features</span><span class="ripple-container"></span></a>
                    </div>
                    <div class="w-btn-wrapper align_none"><a class="w-btn us-btn-style_4"
                                                             href="#counters"><span
                                class="w-btn-label">Learn More</span><span class="ripple-container"></span></a>
                    </div>
                </div>
            </div>
            <div class=" vc_column_container us_animate_afr start">
                <div class="vc_column-inner">
                    <div class="w-image align_center">
                        <div class="w-image-h"><img width="800" height="505" src="./iPhone-6-Infinity1.png"
                                                    class="attachment-large size-large" alt="" loading="lazy"
                                                    srcset="./img/iPhone-6-Infinity1.png 800w, ./img/iPhone-6-Infinity1-600x379.png 600w"
                                                    sizes="(max-width: 800px) 100vw, 800px"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>