<section class="l-section  height_large color_alternate">

    <div class="l-section-h i-cf">
        <div class="g-cols via_grid cols_3 laptops-cols_inherit tablets-cols_inherit mobiles-cols_1 valign_top type_default">
            <div class=" vc_column_container us_animate_afb start">
                <div class="vc_column-inner">
                    <div class="w-iconbox iconpos_left style_circle color_secondary align_left">
                        <div class="w-iconbox-icon" style="font-size:26px;"><i class="material-icons">star</i>
                        </div>
                        <div class="w-iconbox-meta"><h4 class="w-iconbox-title" style="font-size:20px;">Modern
                                Design &amp; Trendy Look</h4>
                            <div class="w-iconbox-text"><p>Lorem ipsum dolor sit amet. Proin non blandit nibh.
                                    Sed eget tortor tincidunt, auctor sem eget.</p>
                            </div>
                        </div>
                    </div>
                    <div class="w-separator size_medium"></div>
                    <div class="w-iconbox iconpos_left style_circle color_secondary align_left">
                        <div class="w-iconbox-icon" style="font-size:26px;"><i
                                class="material-icons">thumb_up</i></div>
                        <div class="w-iconbox-meta"><h4 class="w-iconbox-title" style="font-size:20px;">Focus on
                                Usability &amp; User-experience</h4>
                            <div class="w-iconbox-text"><p>Lorem ipsum dolor sit amet, consec adipiscing elit.
                                    Proin non blandit nibh. Sed eget tortor tincidunt.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=" vc_column_container us_animate_afb start" style="animation-delay:0.2s">
                <div class="vc_column-inner">
                    <div class="w-iconbox iconpos_left style_circle color_secondary align_left">
                        <div class="w-iconbox-icon" style="font-size:26px;"><i
                                class="material-icons">spellcheck</i></div>
                        <div class="w-iconbox-meta"><h4 class="w-iconbox-title" style="font-size:20px;">Advanced
                                Typography Options</h4>
                            <div class="w-iconbox-text"><p>Lorem ipsum dolor sit amet, consec adipiscing elit.
                                    Proin non blandit nibh. Sed eget tortor tincidunt.</p>
                            </div>
                        </div>
                    </div>
                    <div class="w-separator size_medium"></div>
                    <div class="w-iconbox iconpos_left style_circle color_secondary align_left">
                        <div class="w-iconbox-icon" style="font-size:26px;"><i
                                class="material-icons">settings</i></div>
                        <div class="w-iconbox-meta"><h4 class="w-iconbox-title" style="font-size:20px;">Big
                                quantity of Flexible elements</h4>
                            <div class="w-iconbox-text"><p>Lorem ipsum dolor sit amet, consec adipiscing elit.
                                    Proin non blandit nibh. Sed eget tortor tincidunt.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=" vc_column_container us_animate_afb start" style="animation-delay:0.4s">
                <div class="vc_column-inner">
                    <div class="w-iconbox iconpos_left style_circle color_secondary align_left">
                        <div class="w-iconbox-icon" style="font-size:26px;"><i class="material-icons">tablet_android</i>
                        </div>
                        <div class="w-iconbox-meta"><h4 class="w-iconbox-title" style="font-size:20px;">Ultra
                                Responsive &amp; Retina Ready</h4>
                            <div class="w-iconbox-text"><p>Lorem ipsum dolor sit amet. Proin non blandit nibh.
                                    Sed eget tortor tincidunt, auctor sem eget.</p>
                            </div>
                        </div>
                    </div>
                    <div class="w-separator size_medium"></div>
                    <div class="w-iconbox iconpos_left style_circle color_secondary align_left">
                        <div class="w-iconbox-icon" style="font-size:26px;"><i
                                class="material-icons">color_lens</i></div>
                        <div class="w-iconbox-meta"><h4 class="w-iconbox-title" style="font-size:20px;">
                                Extensive Style Options &amp; Unlimited Colors</h4>
                            <div class="w-iconbox-text"><p>Lorem ipsum dolor sit amet, consec adipiscing elit.
                                    Proin non blandit nibh. Sed eget tortor tincidunt.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>