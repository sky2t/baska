<section class="l-section  height_large" id="start">

    <div class="l-section-h i-cf">
        <div class="g-cols via_grid cols_1 laptops-cols_inherit tablets-cols_inherit mobiles-cols_1 valign_top type_default">
            <div class=" vc_column_container">
                <div class="vc_column-inner">
                    <div class="wpb_text_column">
                        <div class="wpb_wrapper"><h2 style="text-align: center;">Zephyr is a Healthy Mixture
                                of</h2>
                        </div>
                    </div>
                    <div class="w-separator size_medium"></div>
                    <div class="g-cols  via_grid cols_3 laptops-cols_inherit tablets-cols_inherit mobiles-cols_1 valign_top type_default">
                        <div class=" vc_column_container us_animate_afc start">
                            <div class="vc_column-inner">
                                <div class="w-iconbox iconpos_top style_circle color_primary align_center"><a
                                        href="#portfolio" class="w-iconbox-link"
                                        aria-label="Passion">
                                        <div class="w-iconbox-icon" style="font-size:2rem;"><i
                                                class="material-icons">favorite_outline</i><span
                                                class="ripple-container"></span></div>
                                    </a>
                                    <div class="w-iconbox-meta"><a
                                            href="#portfolio"
                                            class="w-iconbox-link" aria-label="Passion"><h4
                                                class="w-iconbox-title">Passion</h4></a>
                                        <div class="w-iconbox-text"><p>Lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit. Morbi sagittis, sem quis lacinia faucibus.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" vc_column_container us_animate_afc start"
                             style="animation-delay:0.2s">
                            <div class="vc_column-inner">
                                <div class="w-iconbox iconpos_top style_circle color_primary align_center"><a
                                        href="#simple" class="w-iconbox-link"
                                        aria-label="Technology">
                                        <div class="w-iconbox-icon" style="font-size:2rem;"><i
                                                class="material-icons">phonelink</i><span
                                                class="ripple-container"></span></div>
                                    </a>
                                    <div class="w-iconbox-meta"><a href="#simple"
                                                                   class="w-iconbox-link"
                                                                   aria-label="Technology"><h4
                                                class="w-iconbox-title">Technology</h4></a>
                                        <div class="w-iconbox-text"><p>Lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit. Morbi sagittis, sem quis lacinia faucibus.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" vc_column_container us_animate_afc start"
                             style="animation-delay:0.4s">
                            <div class="vc_column-inner">
                                <div class="w-iconbox iconpos_top style_circle color_primary align_center"><a
                                        href="#counters" class="w-iconbox-link"
                                        aria-label="Simplicity">
                                        <div class="w-iconbox-icon" style="font-size:2rem;"><i
                                                class="material-icons">nature_people</i><span
                                                class="ripple-container"></span></div>
                                    </a>
                                    <div class="w-iconbox-meta"><a href="#counters"
                                                                   class="w-iconbox-link"
                                                                   aria-label="Simplicity"><h4
                                                class="w-iconbox-title">Simplicity</h4></a>
                                        <div class="w-iconbox-text"><p>Lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit. Morbi sagittis, sem quis lacinia faucibus.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>