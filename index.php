<?php
    require_once ("settings.php");
    require_once ("head.php");
?>

<body class="l-body us-core_8.4.1 header_hor headerinpos_top header_hor state_mobiles" itemscope="" itemtype="https://schema.org/WebPage">
<div class="l-canvas type_wide">
<?php require_once ("header.php"); ?>

    <main id="page-content" class="l-main" itemprop="mainContentOfPage">

        <?php
        require_once ("header_section.php");
        require_once ("box_section.php");
        require_once ("details_section.php");
        require_once ("gallery_section.php");
        require_once ("phone_section.php");
        require_once ("counters_section.php");
        require_once ("iconbox_section.php");
        require_once ("50x50_section.php");

        ?>

    </main>
</div>

<?php
require_once ("footer.php");