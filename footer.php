<footer id="page-footer" class="l-footer">
    <section class="l-section  height_large color_footer-top">

        <div class="l-section-h i-cf">
            <div class="g-cols via_grid cols_3 laptops-cols_inherit tablets-cols_inherit mobiles-cols_1 valign_top type_default"
                 style="grid-gap:calc(3rem + 10px);">
                <div class=" vc_column_container">
                    <div class="vc_column-inner">
                        <div class="wpb_text_column">
                            <div class="wpb_wrapper"><h4>Some Text</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis, sem quis
                                    lacinia faucibus, orci ipsum gravida tortor, vel interdum mi sapien ut justo. Nulla
                                    varius consequat magna, id molestie ipsum volutpat quis. Suspendisse consectetur
                                    fringilla suctus.</p>
                            </div>
                        </div>
                        <div class="w-separator size_small"></div>
                        <div class="w-socials color_text shape_circle style_solid hover_fade"
                             style="--gap:0.1em;">
                            <div class="w-socials-list">

                                <?php foreach (SOCIALS as $name=>$social):
                                if($social):
                                ?>

                                    <div class="w-socials-item <?= $name ?>">
                                        <a class="w-socials-item-link"
                                           href="<?= $social ?>"
                                           target="_blank" rel="noopener nofollow"
                                           title="Facebook"
                                           aria-label="<?= $name ?>">
                                            <span class="w-socials-item-link-hover"></span>
                                            <i class="fab fa-<?= $name ?>"></i>
                                            <span class="ripple-container"></span>
                                        </a>
                                        <div class="w-socials-item-popup"><span><?= $name ?></span>
                                        </div>
                                    </div>

                                <?php endif; endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" vc_column_container">
                    <div class="vc_column-inner">
                        <div class="vc_wp_posts wpb_content_element">
                            <div class="widget widget_recent_entries">
                                <h2 class="widgettitle">Recent Posts</h2>
                                <ul>
                                    <li>
                                        <a href="/this-is-a-single-interesting-post/">This
                                            is a Single Interesting Post</a>
                                        <span class="post-date">April 19, 2018</span>
                                    </li>
                                    <li>
                                        <a href="/photography-science/">Photography is the
                                            Science</a>
                                        <span class="post-date">February 27, 2016</span>
                                    </li>
                                    <li>
                                        <a href="/this-post-looks-beautiful-even-with-long-interesting-title/">This
                                            Post Looks Beautiful even with Long Interesting Title</a>
                                        <span class="post-date">September 12, 2015</span>
                                    </li>
                                    <li>
                                        <a href="/this-is-a-sticky-post/">This is a Sticky
                                            Post</a>
                                        <span class="post-date">September 15, 2014</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" vc_column_container">
                    <div class="vc_column-inner">
                        <div class="vc_wp_search wpb_content_element">
                            <div class="widget widget_search"><h2 class="widgettitle">Search Something</h2>
                                <form role="search" method="get" class="search-form"
                                      action="/">
                                    <label>
                                        <span class="screen-reader-text">Search for:</span>
                                        <input type="search" class="search-field" placeholder="Search …" value=""
                                               name="s">
                                    </label>
                                    <input type="submit" class="search-submit" value="Search">
                                </form>
                            </div>
                        </div>
                        <div class="w-separator size_medium"></div>
                        <div class="w-iconbox iconpos_left style_default color_contrast align_left no_text">
                            <div class="w-iconbox-icon" style="font-size:28px;"><i class="material-icons">home</i></div>
                            <div class="w-iconbox-meta"><h4 class="w-iconbox-title" style="font-size:18px;"><?= ADDRESS ?></h4></div>
                        </div>
                        <div class="w-separator size_small"></div>
                        <div class="w-iconbox iconpos_left style_default color_contrast align_left no_text">
                            <div class="w-iconbox-icon" style="font-size:28px;"><i class="material-icons">mail</i></div>
                            <div class="w-iconbox-meta"><h4 class="w-iconbox-title" style="font-size:18px;">
                                    <?= EMAIL ?></h4></div>
                        </div>
                        <div class="w-separator size_small"></div>
                        <div class="w-iconbox iconpos_left style_default color_contrast align_left no_text">
                            <div class="w-iconbox-icon" style="font-size:28px;"><i class="material-icons">phone</i>
                            </div>
                            <div class="w-iconbox-meta"><h4 class="w-iconbox-title" style="font-size:18px;"><?= PHONE ?></h4></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="l-section  align_center_xs height_small color_footer-bottom">

        <div class="l-section-h i-cf">
            <div class="g-cols via_grid cols_2 laptops-cols_inherit tablets-cols_inherit mobiles-cols_1 valign_top type_default">
                  <div class="copyright">
                      <p>© <?php echo date("Y"); ?> All rights reserved.  <?= SITE_NAME ?></p>
                </div>
            </div>
        </div>
    </section>
</footer>
<a class="w-toplink pos_right" href="#" title="Back to top"
   aria-label="Back to top"><span></span></a> <a id="w-header-show" class="w-header-show" aria-label="Menu"
                                                 href="javascript:void(0);"><span>Menu</span></a>
<div class="w-header-overlay"></div>

<script src="./js/jquery.min.js" type="text/javascript"></script>
<script src="./js/js.cookie.min.js" type="text/javascript"></script>
<script src="./js/main.js" type="text/javascript"></script>

<!--<script src="./js/jquery.blockUI.min.js" id="jquery-blockui-js"></script>-->
<!--<script src="./js/zephyr.us-themes.com.js" type="text/javascript" id="us-core-js"></script>-->
</body>
</html>