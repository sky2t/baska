<section class="l-section  height_large color_primary with_img parallax_ver" id="counters">

    <div class="l-section-img"
         style="background-image: url(./img/picjumbo.com_HNCK2634.jpg);"
         data-img-width="1920" data-img-height="1280"></div>
    <div class="l-section-overlay"></div>
    <div class="l-section-h i-cf">
        <div class="g-cols via_grid cols_1 laptops-cols_inherit tablets-cols_inherit mobiles-cols_1 valign_top type_default">
            <div class=" vc_column_container">
                <div class="vc_column-inner">
                    <div class="wpb_text_column">
                        <div class="wpb_wrapper"><h2 style="text-align: center;"><strong>ZEPHYR. </strong>New
                                Edge of the Theme Experience!</h2>
                        </div>
                    </div>
                    <div class="w-separator size_medium"></div>
                    <div class="g-cols  via_grid cols_4 laptops-cols_inherit tablets-cols_inherit mobiles-cols_1 valign_top type_default">
                        <div class=" vc_column_container us_animate_afc start">
                            <div class="vc_column-inner">
                                <div class="w-counter color_heading align_center has_font_size"
                                     data-duration="2000">
                                    <div class="w-counter-value"><span class="w-counter-value-part type_text"
                                                                       data-final=""></span><span
                                            class="w-counter-value-part type_number"
                                            data-final="72">72</span><span
                                            class="w-counter-value-part type_text" data-final="+">+</span></div>
                                    <h6 class="w-counter-title">Button combinations</h6></div>
                            </div>
                        </div>
                        <div class=" vc_column_container us_animate_afc start"
                             style="animation-delay:0.2s">
                            <div class="vc_column-inner">
                                <div class="w-counter us_custom_a832860b color_heading align_center has_font_size"
                                     data-duration="2000">
                                    <div class="w-counter-value"><span class="w-counter-value-part type_text"
                                                                       data-final=""></span><span
                                            class="w-counter-value-part type_number" data-final="250">250</span><span
                                            class="w-counter-value-part type_text" data-final=""></span></div>
                                    <h6 class="w-counter-title">Portfolio combinations</h6></div>
                            </div>
                        </div>
                        <div class=" vc_column_container us_animate_afc start"
                             style="animation-delay:0.4s">
                            <div class="vc_column-inner">
                                <div class="w-counter us_custom_a832860b color_heading align_center has_font_size"
                                     data-duration="2000">
                                    <div class="w-counter-value"><span class="w-counter-value-part type_text"
                                                                       data-final=""></span><span
                                            class="w-counter-value-part type_number" data-final="657">657</span><span
                                            class="w-counter-value-part type_text" data-final=""></span></div>
                                    <h6 class="w-counter-title">Google fonts</h6></div>
                            </div>
                        </div>
                        <div class=" vc_column_container us_animate_afc start"
                             style="animation-delay:0.6s">
                            <div class="vc_column-inner">
                                <div class="w-counter us_custom_a832860b color_heading align_center has_font_size"
                                     data-duration="2000">
                                    <div class="w-counter-value"><span class="w-counter-value-part type_text"
                                                                       data-final=""></span><span
                                            class="w-counter-value-part type_number"
                                            data-final="80">80</span><span
                                            class="w-counter-value-part type_text" data-final="+">+</span></div>
                                    <h6 class="w-counter-title">IconBox combinations</h6></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>